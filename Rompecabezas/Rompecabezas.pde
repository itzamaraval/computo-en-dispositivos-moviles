import android.view.MotionEvent;
import ketai.ui.*;
PImage picture;

 //Paola Valdez y José Treviño

KetaiGesture gesture;
int anchoPieza, altoPieza; 
int vacioX, vacioY; 
int PosX, PosY; 
int seleccionX = -1, seleccionY = -1; 
 
IntList shuffleOrder = new IntList(); 
PImage[] piezas = new PImage[9]; 
int[][] rompecabezas = new int[3][3]; 
boolean seleccionado = false; 
 
int k = 0; 
 
void setup() {
  gesture = new KetaiGesture(this);
  picture = loadImage("zayn.jpg");
  //cambiar valores de imagen y size si se corre en pc, los valores establecidos
  //funcionan para celular
  picture.resize(1500,1500);
  size(1500, 1500);
 
  anchoPieza = picture.width/3;  
  altoPieza = picture.height/3; 
  
  for (int i = 0; i < 9; i++) {
    shuffleOrder.append(i); //llenar array de numeros desrdeados
  }
  for (int y = 0; y < 3; y++) {
    for (int x = 0; x < 3; x++) {
      piezas[k] = picture.get(x*anchoPieza, y*altoPieza, anchoPieza, altoPieza); //cortado de la imagen para las piezas
      k++;
    }
  }
  shuffleOrder.shuffle();//desordenar 
  rellenarRompecabezas();
  quitarPieza();
}
 
void draw() {
  background(248, 168, 188);
  PosX = int(mouseX/anchoPieza);
  PosY = int(mouseY/altoPieza);
 
  k = 0;
  for (int y = 0; y < 3; y++) {
    for (int x = 0; x < 3; x++) {
      if (rompecabezas[x][y] != -1) //se dibujan las piezas del rompecabezas y se quita una
        image(piezas[rompecabezas[x][y]], x*anchoPieza, y*altoPieza);
      k++;
    }
  }

 
  if (seleccionX != -1) {
    contornoPieza(seleccionX, seleccionY); //dibuja el contorno de la pieza seleccionada
  }
}
 
void rellenarRompecabezas() {
  k = 0;
  for (int y = 0; y < 3; y++) {
    for (int x = 0; x < 3; x++) {
      rompecabezas[x][y] = shuffleOrder.get(k); //se llena el rompecabezas con los ìndices del array de numeros desordenados
      k++;
    }
  }
}
 
void quitarPieza() {
  vacioX = int(random(3));
  vacioY = int(random(3));
  rompecabezas[vacioX][vacioY] = -1; //se elimina una pieza del rompecabezas al azar
}
 
void contornoPieza(int x, int y) {
  noFill();
  stroke(255);
  strokeWeight(2);
  rect(x*anchoPieza, y*altoPieza, anchoPieza, altoPieza);
}
 
boolean vacioCerca() { //verifica que la pieza esté arriba, abajo, a la izq o derecha del espacio vacío
  if (((PosX == vacioX-1 || PosX == vacioX+1) && PosY == vacioY)|| ((PosY == vacioY-1 || PosY == vacioY+1) && PosX == vacioX))
    return true;
  else
    return false;
}
 

void onTap(float x, float y){
  println(x,y);
  
}

void mousePressed() {
  if (vacioCerca()) {
    if (seleccionado && PosX == seleccionX && PosY == seleccionY) { //si se da click de nuevo en la pieza seleccionada actualmente, se deselecciona esa pieza
      seleccionX = -1;
      seleccionY = -1;
      seleccionado = false;
    } 
    else { //si aún no hay ninguna selección, se selecciona la pieza en la que se haga click
      seleccionX = PosX;
      seleccionY = PosY;
      seleccionado = true;
    }
  }
  if (seleccionado && PosX == vacioX && PosY == vacioY) {
    rompecabezas[PosX][PosY] = rompecabezas[seleccionX][seleccionY];
    rompecabezas[seleccionX][seleccionY] = -1;  //si hay una pieza seleccionada y después se da click en la vacía se cambian de lugar
    vacioX = seleccionX; //el lugar vacío se cambia a donde estaba la pieza anterior
    vacioY = seleccionY;
    seleccionX = -1;
    seleccionY = -1;
    seleccionado = false;
  }
}


public boolean surfaceTouchEvent(MotionEvent event) {

  //call to keep mouseX, mouseY, etc updated
  super.surfaceTouchEvent(event);

  //forward event to class for processing
  return gesture.surfaceTouchEvent(event);
}
